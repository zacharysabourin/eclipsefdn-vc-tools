@Library('common-shared') _

pipeline {
  agent {
    kubernetes {
      label 'kubedeploy-agent'
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: kubectl
            image: eclipsefdn/kubectl:okd-c1-1.24.2
            command:
            - cat
            tty: true
            resources:
              limits:
                cpu: 1
                memory: 1Gi
            volumeMounts:
            - mountPath: "/home/default/.kube"
              name: "dot-kube"
              readOnly: false
          - name: jnlp
            resources:
              limits:
                cpu: 1
                memory: 1Gi
          volumes:
          - name: "dot-kube"
            emptyDir: {}
      '''
    }
  }

  environment {
    NAMESPACE = 'foundation-internal-webdev-apps'
    IMAGE_NAME = 'eclipsefdn/eclipsefdn-github-sync'
    CONTAINER_NAME = 'eclipsefdn-github-sync'
    GL_IMAGE_NAME = 'eclipsefdn/eclipsefdn-gitlab-sync'
    GL_CONTAINER_NAME = 'eclipsefdn-gitlab-sync'
    BUI_IMAGE_NAME = 'eclipsefdn/eclipsefdn-import-backup'
    BUI_CONTAINER_NAME = 'eclipsefdn-import-backup'
    TAG_NAME = sh(
      script: """
        GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
        printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
      """,
      returnStdout: true
    )
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timeout(time: 30, unit: 'MINUTES') 
  }

  triggers {
    // build once a week to keep up with parents images updates
    cron('H H * * H')
  }

  stages {
    stage('Build docker image') {
      agent {
        label 'docker-build'
      }
      steps {
        sh '''
          docker build --pull -t ${IMAGE_NAME}:${TAG_NAME} -t ${IMAGE_NAME}:latest .
          docker build --pull -t ${GL_IMAGE_NAME}:${TAG_NAME} -t ${GL_IMAGE_NAME}:latest -f Dockerfile.gitlab .
          docker build --pull -t ${BUI_IMAGE_NAME}:${TAG_NAME} -t ${BUI_IMAGE_NAME}:latest -f Dockerfile.import .
        '''
      }
    }

    stage('Push docker image') {
      when {
        branch 'production'
      }
      agent {
        label 'docker-build'
      }
      steps {
        withDockerRegistry([credentialsId: '04264967-fea0-40c2-bf60-09af5aeba60f', url: 'https://index.docker.io/v1/']) {
          sh '''
            docker push ${IMAGE_NAME}:${TAG_NAME}
            docker push ${IMAGE_NAME}:latest
            docker push ${GL_IMAGE_NAME}:${TAG_NAME}
            docker push ${GL_IMAGE_NAME}:latest
            docker push ${BUI_IMAGE_NAME}:${TAG_NAME}
            docker push ${BUI_IMAGE_NAME}:latest
          '''
        }
      }
    }

    stage('Deploy to cluster') {
      when {
        branch 'production'
      }
      steps {
        container('kubectl') {
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=github-sync",
            kind: "cronjob.v1.batch",
            containerName: "${env.CONTAINER_NAME}",
            newImageRef: "${env.IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=gitlab-sync",
            kind: "cronjob.v1.batch",
            containerName: "${env.GL_CRONJOB_NAME}",
            newImageRef: "${env.GL_IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=github-org-backup",
            kind: "cronjob.v1.batch",
            containerName: "${env.BUI_CONTAINER_NAME}",
            newImageRef: "${env.BUI_IMAGE_NAME}:${env.TAG_NAME}"
          ])
        }
      }
    }
  }

  post {
    always {
      deleteDir() /* clean up workspace */
      sendNotifications currentBuild
    }
  }
}
